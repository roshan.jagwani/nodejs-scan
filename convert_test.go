package main

import (
	"bytes"
	"encoding/json"
	"os"
	"strings"
	"testing"

	log "github.com/sirupsen/logrus"

	"github.com/stretchr/testify/require"
)

func TestConvert(t *testing.T) {
	tests := []struct {
		description  string
		projectPath  string
		reportPath   string
		wantReport   string
		in           string
		wantErrorLog string
	}{
		{
			description: "basic njsscan report",
			reportPath:  "testdata/njsscanReports/njsscan.json",
			wantReport:  "testdata/expect/gl-sast-report.json",
		},
		{
			description: "basic njsscan report with projectPath set",
			projectPath: "foo/bar/baz",
			reportPath:  "testdata/njsscanReports/njsscan-with-project-path.json",
			wantReport:  "testdata/expect/gl-sast-report.json",
		},
		{
			description: "basic njsscan report with incorrect matchlines",
			reportPath:  "testdata/njsscanReports/njsscan-bad-matchlines.json",
			wantReport:  "testdata/expect/gl-sast-report-bad-matchlines.json",
		},
		{
			description:  "basic njsscan report with error messages",
			reportPath:   "testdata/njsscanReports/njsscan-with-errors.json",
			wantReport:   "testdata/expect/gl-sast-report-with-errors.json",
			wantErrorLog: "▶ njsscan error: SourceParseError, Could not parse .njsscan as javascript",
		},
	}
	log.SetLevel(log.DebugLevel)

	for _, test := range tests {
		t.Run(test.description, func(t *testing.T) {
			t.Log(test.description)
			f, err := os.Open(test.reportPath)
			require.NoError(t, err)

			defer f.Close()

			buf := new(bytes.Buffer)
			log.SetOutput(buf)
			report, err := convert(f, test.projectPath)
			require.NoError(t, err)

			wantReportBytes, err := os.ReadFile(test.wantReport)
			require.NoError(t, err)

			gotReportBytes, err := json.MarshalIndent(report, "", "  ")
			require.NoError(t, err)

			gotErrorLog := buf.String()

			require.Equal(t, bytes.Trim(wantReportBytes, "\n"), bytes.Trim(gotReportBytes, "\n"))
			if test.wantErrorLog != "" {
				require.Contains(t, strings.TrimSpace(gotErrorLog), test.wantErrorLog)
			}
		})
	}
}
